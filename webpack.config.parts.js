// @flow
// Node.js does not support import/export (even with the Babel register hook)
const path = require('path')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports.configureDevServer = ({ contentBase, host, port }) => ({
  devServer: {
    compress: true,
    contentBase,
    historyApiFallback: true,
    host,
    overlay: {
      errors: true,
      warnings: true,
    },
    port,
  },
})

module.exports.copyAssets = ({ contentBase }) => ({
  plugins: [
    new CopyWebpackPlugin([
      { from: contentBase },
    ]),
  ],
})

module.exports.generateHtml = ({ template = path.resolve(__dirname, 'node_modules', 'html-webpack-template', 'index.ejs'), ...options }) => ({
  plugins: [
    new HtmlWebpackPlugin({
      template,
      ...options,
    }),
  ],
})

module.exports.generateSourceMaps = ({ type }) => ({
  devtool: type,
})

module.exports.lintJavaScript = ({ test, include, exclude, ...options }) => ({
  module: {
    rules: [
      {
        test,
        include,
        exclude,
        enforce: 'pre',
        use: [
          {
            loader: 'eslint-loader',
            options,
          },
        ],
      },
    ],
  },
})

module.exports.transpileJavaScript = ({ test, include, exclude }) => ({
  module: {
    rules: [
      {
        test,
        include,
        exclude,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
    ],
  },
})
