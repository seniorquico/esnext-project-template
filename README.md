# esnext-project-template

This template represents the starting point for my modern JavaScript projects. At a high level, it makes use of the following packages:

* [Babel](https://babeljs.io/),
* [ESLint](http://eslint.org/),
* [Flow](https://flow.org/),
* [React+ReactDOM](https://facebook.github.io/react/),
* [standard](https://standardjs.com/), and
* [webpack](https://webpack.js.org/).

[Babel](https://babeljs.io/) is configured to generate ES2017 (natively supported by latest version of Chrome) and includes experimental support for [class properties](https://babeljs.io/docs/plugins/transform-class-properties/) and the [object rest/spread operators](https://babeljs.io/docs/plugins/transform-object-rest-spread/).

[ESLint](http://eslint.org/) is configured to enforce the [JavaScript Standard Style](https://standardjs.com/) with a non-standard requirement for [comma-dangle](http://eslint.org/docs/rules/comma-dangle) and [flowtype/require-valid-file-annotation](https://github.com/gajus/eslint-plugin-flowtype#eslint-plugin-flowtype-rules-require-valid-file-annotation). Run ESLint using `yarn run lint`/`npm run-script lint`.

[webpack](https://webpack.js.org/) is configured to generate a production build and run a development web server. Run webpack using `yarn run build`/`npm run-script build` to generate the production build. Run `yarn run start-production`/`npm run-script start-production` to run the development web server with the production build. Run `yarn run start-development`/`npm run-script start-development` to run the development web server with a non-production build generated by webpack-dev-server.
