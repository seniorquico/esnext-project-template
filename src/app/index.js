// @flow
import React from 'react'
import ReactDOM from 'react-dom'

const App = () => (
  <div>
    <h1>Application</h1>
    <div>Hello, world!</div>
  </div>
)

ReactDOM.render(<App />, document.getElementById('app'))
