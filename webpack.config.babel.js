// @flow
// Node.js does not support import/export (even with the Babel register hook)
const merge = require('webpack-merge')
const path = require('path')
const parts = require('./webpack.config.parts.js')

const htmlTemplateOptions = {
  appMountId: 'app',
  template: path.resolve(__dirname, 'src', 'index.ejs'),
  title: 'Application',
}

const jsRuleCondition = {
  test: /\.js$/,
  include: path.resolve(__dirname, 'src', 'app'),
  exclude: /(node_modules|bower_components)/,
}

const baseConfig = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    app: path.resolve(__dirname, 'src', 'app', 'index.js'),
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'src', 'app'),
      'node_modules',
    ],
  },
}

module.exports = (env) => {
  if (env == null) {
    env = process.env.NODE_ENV === 'production' ? 'production' : 'development'
  }

  if (env === 'production') {
    return merge([
      baseConfig,
      parts.lintJavaScript(jsRuleCondition),
      parts.transpileJavaScript(jsRuleCondition),
      parts.generateSourceMaps({
        type: 'source-map',
      }),
      parts.generateHtml(htmlTemplateOptions),
      parts.copyAssets({
        contentBase: path.resolve(__dirname, 'src', 'assets'),
      }),
    ])
  } else {
    return merge([
      baseConfig,
      {
        output: {
          pathinfo: true,
        },
      },
      parts.lintJavaScript(jsRuleCondition),
      parts.transpileJavaScript(jsRuleCondition),
      parts.generateSourceMaps({
        type: 'inline-source-map',
      }),
      parts.generateHtml(htmlTemplateOptions),
      parts.configureDevServer({
        contentBase: path.resolve(__dirname, 'src', 'assets'),
        host: process.env.HOST,
        port: process.env.PORT,
      }),
    ])
  }
}
